console.log('Hello World')

function add(n1, n2) {
	const sum = n1 + n2
	console.log('Displayed sum of '+n1+' and '+n2)
	console.log(sum)
}

function subt(n1, n2) {
	const diff = n1 - n2
	console.log('Displayed difference of '+n1+' and '+n2)
	console.log(diff)
}

add(5, 15)
subt(20, 5)

function mult(n1, n2) {
	return n1 * n2
}

function divide(n1, n2) {
	return n1 / n2
}

const n1 = 50
const n2 = 10
const product = mult(n1, n2)
const quotient = divide(n1, n2)

console.log('The product of '+n1 +' and '+ n2)
console.log(product)
console.log('The quotient of '+n1 +' and '+ n2)
console.log(quotient)

function area(radius) {
	return 3.14159 * (radius*radius)
}

const radius = 15
const circleArea = area(radius)
console.log('The result of getting the area of a circle with '+ radius+':')
console.log(parseFloat(circleArea.toFixed(2)))

function average(n1, n2, n3, n4) {
	return (n1+n2+n3+n4)/4
}

const averageVar = average(20, 40, 60, 80)
console.log('The average of 20, 40, 60, 80:')
console.log(averageVar)

function checkPassingScore(score, total) {
	const perc = (score / total) * 100

	return perc > 75
}

const isPassingScore = checkPassingScore(38, 50)
console.log('Is 38/50 a passing score?')
console.log(isPassingScore)